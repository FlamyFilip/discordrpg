﻿using Discord.Rest;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordRPG
{
    public class Player
    {
        public SocketGuildUser User
        {
            get;
            private set;
        }

        public RestTextChannel RestChannel
        {
            get;
            private set;
        }




        public Player(SocketGuildUser user, RestTextChannel restChannel)
        {
            User = user;
            RestChannel = restChannel;
        }
    }
}
