﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DiscordRPG
{
    public class Program
    {
        public static DiscordSocketClient Client
        {
            get;
            private set;
        }

        public static SocketGuild Guild
        {
            get;
            internal set;
        }

        public static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string tokenPath = currentDirectory + "/token";

            if (!File.Exists(tokenPath))
            {
                Console.WriteLine("Token file not found.");
                File.Create(tokenPath).Close();
                Console.WriteLine("Token file created. Populate it with the bot's token.");
                Console.ReadKey();
                Environment.Exit(1);
            }

            string token = File.ReadAllText(tokenPath);

            Client = new DiscordSocketClient();
            Client.Log += Log;

            try
            {
                await Client.LoginAsync(TokenType.Bot, token);
                await Client.StartAsync();
            }
            catch
            {
                Console.WriteLine("Invalid token. Check your token file!");

                Console.ReadKey();
                Environment.Exit(1);
            }

            Client.MessageReceived += MessageDecoder.MessageReceivedAsync;

            await Task.Delay(-1);
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(string.Format("[Bot] {0}", msg.ToString()));
            return Task.CompletedTask;
        }

    }
}