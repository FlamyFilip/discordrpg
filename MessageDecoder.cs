﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordRPG
{
    public static class MessageDecoder
    {
        public static async Task MessageReceivedAsync(SocketMessage message)
        {
            if (!(message.Channel is SocketTextChannel) || message.Author.IsBot)
                return;
                
            switch (message.Channel.Name)
            {
                case "general":
                    await GeneralMessageAsync(message);
                    break;

                case "private":
                    await PrivateMessageAsync(message);
                    break;
            }
        }

        private static async Task GeneralMessageAsync(SocketMessage message)
        {
            var user = message.Author as SocketGuildUser;
            if (!user.GuildPermissions.Administrator)
                return;

            var command = message.Content.ToLower();

            if (command.StartsWith("setup"))
               await Adventure.Setup(message);

            await message.DeleteAsync();
        }

        private static async Task PrivateMessageAsync(SocketMessage message)
        {
        }
    }
}
