﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordRPG
{
    public static class Adventure
    {
        private static bool isReady = false;
        public static Dictionary<SocketGuildUser, Player> Players
        {
            get;
            private set;
        }

        public static async Task Setup(SocketMessage message)
        {
            if (isReady)
                return;

            isReady = true;
            Program.Guild = (message.Channel as SocketTextChannel).Guild;
            Players = new Dictionary<SocketGuildUser, Player>();

            foreach (SocketTextChannel textChannel in Program.Guild.TextChannels)
            {
                if(textChannel.Name == "private" || textChannel.Name == "data")
                    await textChannel.DeleteAsync();
            }

            var everyone = Program.Guild.EveryoneRole;
            await CreateDataChannel(everyone);

            foreach (SocketGuildUser user in Program.Guild.Users)
            {
                if (user.IsBot)
                    continue;
                
                var restChannel = await Program.Guild.CreateTextChannelAsync("private");
                var perms = OverwritePermissions.DenyAll(restChannel);
                await restChannel.AddPermissionOverwriteAsync(everyone, perms);
                perms = perms.Modify(readMessageHistory: PermValue.Allow, readMessages: PermValue.Allow, sendMessages: PermValue.Allow, embedLinks: PermValue.Allow, attachFiles: PermValue.Allow, addReactions: PermValue.Allow, useExternalEmojis: PermValue.Allow);
                await restChannel.AddPermissionOverwriteAsync(user, perms);
                Players[user] = new Player(user, restChannel);
            }
        }

        private static async Task CreateDataChannel(IRole everyone)
        {
            var channel = await Program.Guild.CreateTextChannelAsync("data");
            var perms = OverwritePermissions.DenyAll(channel);
            await channel.AddPermissionOverwriteAsync(everyone, perms);
            DataManager.RestChannel = channel;
        }
    }
}
